/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegrambot;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

/**
 *
 * @author developer4
 */
public class TelegramBot extends TelegramLongPollingBot {

    public static HttpClient client;
    userDao userDao;
    userChatDao userChatDao;
    topicDao topicDao;

    static ApplicationContext appContext = new ClassPathXmlApplicationContext("appcontext.xml");

    static {

        ServiceUnavailableRetryStrategy t = new ServiceUnavailableRetryStrategy() {
            @Override
            public boolean retryRequest(HttpResponse hr, int i, HttpContext hc) {
                int statusCode = hr.getStatusLine().getStatusCode();
                return statusCode != 302 && i < 4; //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public long getRetryInterval() {
                return 2000; //To change body of generated methods, choose Tools | Templates.
            }
        };
        client = HttpClients.custom()
                .setServiceUnavailableRetryStrategy(t)
                .build();
    }

    @Override
    @Transactional
    public void onUpdateReceived(Update update) {

        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            String returnText = "Введите логин/пароль";
            String userName = update.getMessage().getChat().getFirstName() + " " + update.getMessage().getChat().getLastName();
            long chat_id = update.getMessage().getChatId();
            String telegramAccount = update.getMessage().getChat().getFirstName();
            ReplyKeyboard markup = null;
            if (update.getMessage().getText().equals("/deletepositive")) {

            } else if (update.getMessage().getText().equals("/start")) {
                SendMesage(chat_id, returnText, markup);
            } else {
                if (update.getMessage().getText().length() > 2) {
                    String[] authorise = update.getMessage().getText().split("/");

                    if (authorise.length > 1) {
                        authorise[0] = authorise[0].replace(" ", "");
                        authorise[1] = authorise[1].replace(" ", "");
                        String result = getAccessToken(authorise[0], authorise[1]);
                        if (!result.isEmpty()) {
                            try {
                                userDao = (userDao) appContext.getBean("UserDao");
                                UUID t = userDao.findByLogin(authorise[0]).getId();
                                topicDao = (topicDao) appContext.getBean("TopicDao");
                                for (int i = 0; i < topicDao.getAllByUserId(t).size(); i++) {
                                    userChatDao = (userChatDao) appContext.getBean("UserChatDao");
                                    UUID res = userChatDao.saveChatId(t, (int) (long) chat_id, topicDao.getAllByUserId(t).get(i).getId(), telegramAccount);
                                }
                                returnText = "Уважаемый(-ая), " + userName + ". Вы подписались на уведомления по телеграм. Настроить вы сможете в кабинете системы. Ваше имя пользователя в настройках:" + telegramAccount;
                            } catch (Exception e) {
                                returnText = "Уважаемый(-ая), " + userName + ". Вы уже подписаны на уведомления по телеграм. Настроить вы сможете в кабинете системы. Ваше имя пользователя в настройках:" + telegramAccount;
                            }
                        }
                        SendMesage(chat_id, returnText, markup);
                    } else {
                        SendMesage(chat_id, returnText, markup);
                    }
                } else {
                    SendMesage(chat_id, returnText, markup);
                }
            }
        }
    }

    @Override
    public String getBotUsername() {
        return "AlemresBot";
    }

    @Override
    public String getBotToken() {
        return "564309724:AAH0LAcF0WwPQLp6iPn1zDRPPnJTppRbWoA";
    }

    public void SendMesage(long chatId, String msgText, ReplyKeyboard markup) {
        SendMessage message = new SendMessage() // Create a message object object
                .setChatId(chatId)
                .setText(msgText)
                .setReplyMarkup(markup);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public static String getAccessToken(String login, String password) {
        String url = "http://cabinet.alem.kz:8080";
        HttpPost authorize = new HttpPost(url + "/auth-server/login.do?username=" + login + "&password=" + password);
        HttpPost accestoken = new HttpPost(String.format("%1$s/auth-server/oauth/authorize?client_id"
                + "=webapp&response_type=token&redirect_uri=%1$s", url));
        String result = "";
        try {
            client.execute(authorize);
            HttpResponse response = client.execute(accestoken);
            String location = response.getFirstHeader("Location").getValue();
            if (location != null) {
                String param = "access_token=";
                result = location.substring(location.indexOf(param)
                        + param.length(), location.indexOf("&"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            //log.error(ex);
        } finally {
            authorize.releaseConnection();
            accestoken.releaseConnection();
        }
        return result;
    }

}
