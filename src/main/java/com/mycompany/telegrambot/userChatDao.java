/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegrambot;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import kz.alem.semantics.sql.orm.dao.UserChatDao;

/**
 *
 * @author developer4
 */
@Component("UserChatDao")
public class userChatDao {
    @Autowired
    UserChatDao  userChatDao;
    
    @Transactional
    public UUID saveChatId( UUID userId, int chatId, UUID  topicId, String userName){
        return      userChatDao.saveChatId(userId, chatId , topicId , userName);
    }
}
