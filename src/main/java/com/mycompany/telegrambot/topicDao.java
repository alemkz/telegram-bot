/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegrambot;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;
import kz.alem.semantics.sql.orm.dao.TopicDao;
import kz.alem.semantics.sql.orm.model.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author developer4
 */
@Component("TopicDao")
public class topicDao implements Serializable {

    @Autowired
    TopicDao topicDao;

    @Transactional
    public List<Topic> getAllByUserId(UUID userId) {
        return topicDao.getAllByUserId(userId);
    }
    
}
