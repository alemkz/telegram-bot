/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.telegrambot;

import java.io.Serializable;
import javax.transaction.Transactional;
import kz.alem.semantics.sql.orm.dao.UserActionDao;
import kz.alem.semantics.sql.orm.dao.UserDao;
import kz.alem.semantics.sql.orm.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author developer4
 */
@Component("UserDao")
public class userDao implements Serializable {

    @Autowired
    UserDao userDao;
    
    @Autowired
    UserActionDao userActionDao;

    @Transactional
    public User findByLogin(String login) {
        return userDao.findByLogin(login);
    }
}
